#include "tabuleiro.h"

Peca * Tabuleiro::get_peca(int a,int b)
{
    return tab[a][b];
}

void Tabuleiro::show()
{
    for(int i=0;i<length;i++)
    {
        for(int j=0;j<length;j++)
        {
            Peca *peca = get_peca(i,j);
            char letra =((i+j)%2==0) ? '\xB2' : ' ';
            if(peca){
                if(peca->getPlayer()==P1)
                    letra='X';
                else
                    letra='Y';
            }
            //quadrado branco    \xB2 é um caracter branco
            if ((i+j)%2==0)
                cout<<"\xB2\xB2"<<letra<<"\xB2\xB2";
            else //quadrado preto
                cout<<"  "<<letra<<"  ";
        }
        cout<<endl;
    }
    cout<<heuristica();
    cout<<endl;
}

Jogador *Tabuleiro::getJogador(char id){
    if(id==P1)
        return X;
    else
        return Y;
}

void Tabuleiro::show_childs()
{
   for(list<Tabuleiro *>::iterator p=(*child).begin(); p != (*child).end(); ++p)
        (*p)->show();
}

void Tabuleiro::move_peca(int x,int y,int x_dest,int y_dest){
    tab[x_dest][y_dest]=tab[x][y];
    tab[x_dest][y_dest]->set_values(x_dest,y_dest);
    tab[x][y]=NULL;
    if((tab[x_dest][y_dest]->getPlayer()==P1)&&((x_dest)==length-1))
       tab[x_dest][y_dest]->set_dama();
    else if((tab[x_dest][y_dest]->getPlayer()==P2)&&((x_dest)==0))
       tab[x_dest][y_dest]->set_dama();
}

void Tabuleiro::captura_peca(int x,int y,int x_dest,int y_dest){

    int new_y_dest;
    if(y_dest<y)
        new_y_dest=y_dest-1;
    else
        new_y_dest=y_dest+1;

    if(tab[x][y]->getPlayer()==P1)
    {
        tab[x_dest+1][new_y_dest]=tab[x][y];
        tab[x_dest+1][new_y_dest]->set_values(x_dest+1,new_y_dest);
        tab[x][y]=NULL;
        tab[x_dest][y_dest]->set_values(-1,-1);
        tab[x_dest][y_dest]=NULL;
        if((x_dest+1)==length-1)
            tab[x_dest+1][new_y_dest]->set_dama();
        if((x_dest)==length-1)
            tab[x_dest+1][new_y_dest]->set_dama();
    }
    else
    {
        tab[x_dest-1][new_y_dest]=tab[x][y];
        tab[x_dest-1][new_y_dest]->set_values(x_dest-1,new_y_dest);
        tab[x][y]=NULL;
        tab[x_dest][y_dest]->set_values(-1,-1);
        tab[x_dest][y_dest]=NULL;
        if((x_dest-1)==0)
            tab[x_dest-1][new_y_dest]->set_dama();
        if((x_dest)==length-1)
            tab[x_dest-11][new_y_dest]->set_dama();
    }
}

list<Tabuleiro *> * Tabuleiro::getChilds(int player)
{
    list<Tabuleiro *> *childs = new list<Tabuleiro *>();
    if(player==P1)
    {
        Jogador *j = getJogador(P1);
        list<Peca *> a = j->getPecas();
        for(list<Peca *>::iterator p=a.begin(); p != a.end(); ++p)
        {
            int x=(*p)->get_x();
            int y=(*p)->get_y();
            if(x!=-1)
            {
                if(x<7)
                {
                    if((y-1)>=0)
                    {
                        if(tab[x+1][y-1]==NULL)
                        {
                            Tabuleiro *novo = new Tabuleiro();
                            novo->copiaTabuleiro(this);
                            novo->move_peca(x,y,x+1,y-1);
                            childs->push_back(novo);
                        }
                        else if(tab[x+1][y-1]->getPlayer()==P2)
                        {
                            if(((x+2)<=7)&&((y-2)>=0))
                            {
                                if(tab[x+2][y-2]==NULL)
                                {
                                    Tabuleiro *novo = new Tabuleiro();
                                    novo->copiaTabuleiro(this);
                                    novo->captura_peca(x,y,x+1,y-1);
                                    childs->push_back(novo);

                                }
                            }
                        }
                    }
                    if((y+1)<=7)
                    {
                        if(tab[x+1][y+1]==NULL)
                        {
                            Tabuleiro *novo = new Tabuleiro();
                            novo->copiaTabuleiro(this);
                            novo->move_peca(x,y,x+1,y+1);
                            childs->push_back(novo);
                        }
                        else if(tab[x+1][y+1]->getPlayer()==P2)
                        {
                            if(((x+2)<=7)&&((y+2)<=7))
                            {
                                if(tab[x+2][y+2]==NULL)
                                {
                                    Tabuleiro *novo = new Tabuleiro();
                                    novo->copiaTabuleiro(this);
                                    novo->captura_peca(x,y,x+1,y+1);
                                    childs->push_back(novo);

                                }
                            }
                        }
                    }
                }
            }
        }
    }
    else
    {
        Jogador *j = getJogador(P2);
        list<Peca *> a = j->getPecas();
        for(list<Peca *>::iterator p=a.begin(); p != a.end(); ++p)
        {
            int x=(*p)->get_x();
            int y=(*p)->get_y();
            if(x!=-1)
            {
                if(x>0)
                {
                    if((y-1)>=0)
                    {
                        if(tab[x-1][y-1]==NULL)
                        {
                            Tabuleiro *novo = new Tabuleiro();
                            novo->copiaTabuleiro(this);
                            novo->move_peca(x,y,x-1,y-1);
                            childs->push_back(novo);
                        }
                        else if(tab[x-1][y-1]->getPlayer()==P1)
                        {
                            if(((x-2)>=0)&&((y-2)>=0))
                            {
                                if(tab[x-2][y-2]==NULL)
                                {
                                    Tabuleiro *novo = new Tabuleiro();
                                    novo->copiaTabuleiro(this);
                                    novo->captura_peca(x,y,x-1,y-1);
                                    childs->push_back(novo);

                                }
                            }
                        }
                    }
                    if((y+1)<=7)
                    {
                        if(tab[x-1][y+1]==NULL)
                        {
                            Tabuleiro *novo = new Tabuleiro();
                            novo->copiaTabuleiro(this);
                            novo->move_peca(x,y,x-1,y+1);
                            childs->push_back(novo);
                        }
                        else if(tab[x-1][y+1]->getPlayer()==P1)
                        {
                            if(((x-2)>=0)&&((y+2)<=7))
                            {
                                if(tab[x-2][y+2]==NULL)
                                {
                                    Tabuleiro *novo = new Tabuleiro();
                                    novo->copiaTabuleiro(this);
                                    novo->captura_peca(x,y,x-1,y+1);
                                    childs->push_back(novo);
                                }
                            }
                        }
                    }
                }
            }
        }
    }
    child=childs;
    return childs;
}

void Tabuleiro::copiaTabuleiro(Tabuleiro *t)
{
    X=new Jogador(P1);
    Y=new Jogador(P2);
    for(int i=0;i<length;i++){
        for(int j=0;j<length;j++){
            if(t->tab[i][j]==NULL)
                tab[i][j]=NULL;
            else{
                tab[i][j]=new Peca(i,j,t->tab[i][j]->getPlayer(),t->tab[i][j]->getDama());
                if(t->tab[i][j]->getPlayer()==P1)
                    X->addPeca(tab[i][j]);
                else
                    Y->addPeca(tab[i][j]);
            }
        }
    }
}

int Tabuleiro::heuristica()
{
    Jogador *jA = getJogador(P1);
    Jogador *jB = getJogador(P2);
    list<Peca *> Xpeca = jA->getPecas();
    list<Peca *> Ypeca = jB->getPecas();

    int x_val = 0;
    int y_val = 0;

    for(list<Peca *>::iterator p=(Xpeca).begin(); p != (Xpeca).end(); ++p)
    {
        if((*p)->get_x()!=-1)
            x_val=x_val+(*p)->getVal();
    }

    for(list<Peca *>::iterator p=(Ypeca).begin(); p != (Ypeca).end(); ++p)
    {
        if((*p)->get_x()!=-1)
            y_val=y_val+(*p)->getVal();
    }
    int pontuacao=x_val-y_val;
    return pontuacao;
}

Tabuleiro::Tabuleiro()
{
    X=new Jogador(P1);
    Y=new Jogador(P2);
    length=TAB_SIZE;
    for(int i=0;i<length;i++){
        for(int j=0;j<length;j=j+2){
            if((i!=3)&&(i!=4)){
                if(i<3){
                    if (i%2==0){
                        tab[i][j+1]=new Peca(i,j+1,P1,0);
                        X->addPeca(tab[i][j+1]);
                    }
                    else{
                        tab[i][j]=new Peca(i,j,P1,0);
                        X->addPeca(tab[i][j]);
                    }
                }
                else{
                    if (i%2==0) {
                        tab[i][j+1]=new Peca(i,j+1,P2,0);
                        Y->addPeca(tab[i][j+1]);
                    }
                    else{
                        tab[i][j]=new Peca(i,j,P2,0);
                        Y->addPeca(tab[i][j]);
                    }
                }
            }
            else
                tab[i][j]=NULL;
        }
    }
}

Tabuleiro * Tabuleiro::verificaChild(Jogador *player)
{
    while(true){
        cout<<"digite o movimento"<<endl;
        int x,y;
        int x_dest,y_dest;
        cin>>x;cin>>y;cin>>x_dest;cin>>y_dest;
        if((tab[x][y]->getPlayer()==player->getId()))
        {
            for(list<Tabuleiro *>::iterator p=(*child).begin(); p != (*child).end(); ++p)
                if(((*p)->tab[x][y]==NULL)&&((*p)->tab[x_dest][y_dest]->getPlayer()==player->getId()))
                    return (*p);
        }
    }
}

int Tabuleiro::estadoTerminal(int depth,Jogador *p)
{
    list<Tabuleiro *> *l = getChilds(p->getId());
    if((l->size()==0)||(depth==DEPTH))
        return 1;
    else
        return 0;
}
