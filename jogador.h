#include "peca.h"
#include <list>

class Jogador{
  protected:
    char id;
    list<Peca *> pecas;
  public:
  	int getId();
    void addPeca(Peca *p);
    list<Peca *> getPecas();
    Jogador(char identifier);
};