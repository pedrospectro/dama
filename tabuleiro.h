#include "jogador.h"

#define DEPTH 4
#define TAB_SIZE 8

class Tabuleiro {
  private:
    int length;
    Jogador *X;
    Jogador *Y;
    Peca *tab[TAB_SIZE][TAB_SIZE];
    list<Tabuleiro *> *child;
    int pontuacao;
  public:
  	/*mostra tabuleiro*/
  	void show();

    void copiaTabuleiro(Tabuleiro *t);

  	/*captura peca na coordenada*/
  	Peca *get_peca(int a,int b);

  	Jogador *getJogador(char id);


  	list<Tabuleiro *> *getChilds(int player);
    void show_childs();
    int heuristica();
    void move_peca(int x,int y,int x_dest,int y_dest);
    void captura_peca(int x,int y,int x_dest,int y_dest);
    int estadoTerminal(int depth,Jogador *P);
    Tabuleiro * verificaChild(Jogador *p);
    Tabuleiro();
};
