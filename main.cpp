#include "tabuleiro.h"

//fun��o principal com recurso de chamada do tabuleiro e das pe�as
//al�m das poss�veis jogadas

int main()
{
    Tabuleiro *t = new Tabuleiro();//instanciando objeto tabuleiro com ponteiro
    Jogador *A = t->getJogador(P1);//instanciando jogadores A e B
    Jogador *B = t->getJogador(P2);
    Jogador *ativo = A;

    while(!t->estadoTerminal(0,ativo))
    {
        t->show();
        t->getChilds(ativo->getId());
        t=t->verificaChild(ativo);
        if(ativo==A)
            ativo=B;
        else
            ativo=A;

    }
    return 0;
}
