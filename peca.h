#include <iostream>
#include <stdlib.h>

#define P1 0
#define P2 1
using namespace std;

class Peca{
protected:
    char x,y;
    char dama;
    char player;
  public:
    /*muda coordenada da peca*/
    void set_values (char a,char b);
    /*captura coordenadas da peca*/
    int get_x();
    int get_y();
    /*sinaliza dama feita*/
    void set_dama();
    /*captura pontuacao da peca*/
    int getVal();
    int getPlayer();
    char getDama();
    Peca(char a,char b,char p,char d);
};
