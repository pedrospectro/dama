#include "peca.h"

void Peca::set_values (char a,char b)
{
	x=a;y=b;
}

int Peca::get_x(){return x;}

int Peca::get_y(){return y;}

char Peca::getDama(){
    return dama;
}

Peca::Peca(char a,char b,char p,char d)
{
	dama=d;player=p;
	set_values(a,b);
}

int Peca::getVal(){
	if(!dama) return 1;
	else return 5;
}

int Peca::getPlayer(){
	return player;
}

void Peca::set_dama()
{
	dama=1;
}
