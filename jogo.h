#include "tabuleiro.h"

#define DEPTH 4

class Jogo {
  private:
   int active_player;
	 Tabuleiro *t;
   int val;
  public:
    list<Tabuleiro *> getChilds();//
    int estadoTerminal();
    int heuristica();
    void setVal(int v);
    void movePeca(Peca *p,int x,int b);
    int miniMax(Jogo *j,int depth);
    Jogo(Tabuleiro *t,int p);
};